#include <eigen3/Eigen/Dense>
#include <math.h>
#include <utility>
#include <complex>

template<typename T, size_t N>
class dipoleMoM {
public:
  using Vect = Eigen::Matrix<std::complex<T>, N, 1>;
  using Mat = Eigen::Matrix<std::complex<T>, N, N>;
  
protected:
  T length;
  T width;
  T ang_freq;
  T epsR;
  T delta;
  static constexpr T eps0 = 8.854187817e-12;
  static constexpr T c0 = 299792458;
  Vect V;
  Vect I;
  Mat Z;

  void computeMatrix();
  
public:  
  dipoleMoM(T l, T w,T freq, T epsR = 1.0): length(l), width(w), ang_freq(2*M_PI*freq), epsR(epsR), delta(length/N){};
  const Vect& solve(T v0);
  const Vect& getV(){ return std::move(V); };
  const Vect& getI(){ return std::move(I); };
  const Mat& getZ(){ return std::move(Z); };
};

template<typename T, size_t N>
void dipoleMoM<T, N>::computeMatrix() {
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++) {
      T R = sqrt(width*width + ((j - i)*delta)*((j - i)*delta));
      T beta = ang_freq*sqrt(epsR)/c0;
      Z(i, j) = (std::complex<T>(0, 1)*delta/(ang_freq*epsR*eps0))*delta*std::polar<T>(1, -beta*R)*((beta*width*R)*(beta*width*R) + (std::complex<T>(1, beta*R))*(2*R*R - 3*width*width*width))/(4*T(M_PI)*R*R*R*R*R);
    }
}

template<typename T, size_t N>
const typename dipoleMoM<T, N>::Vect& dipoleMoM<T, N>::solve(T v0) {
  V = Vect::Zero();
  if(N%2 != 0)
    V(N/2) = v0;
  else
    V(N/2) = V(N/2 + 1) = v0/2;
    
  computeMatrix();
  
  I = Z.colPivHouseholderQr().solve(V);
  
  return I;
}
