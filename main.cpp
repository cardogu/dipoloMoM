#include "dipoleMoM.hh"
#include <iostream>
#include <fstream>

int main(){

  //speed of light
  constexpr double c0 = 299792458.0;
  
  //frequency
  constexpr double freq = 1e6;
  //dipole length in wavelengths
  constexpr double l = 0.5*(c0/freq);
  //dipole thickness
  constexpr double a = 1e-3*(c0/freq);
  //number of segments
  constexpr size_t N = 51;

  dipoleMoM<double, N> dip(l, a, freq);

  auto I =  dip.solve(1.0);

  std::cout << "I:" << std::endl << I << std::endl;
  // std::cout << "V:" << std::endl << dip.getV() << std::endl;
  // std::cout << "Z:" << std::endl << dip.getZ() << std::endl;

  std::ofstream arquivo;
  arquivo.open("Ireal.txt");
  arquivo << I.real() << std::endl;
  arquivo.close();

  arquivo.open("Iimag.txt");
  arquivo << I.imag() << std::endl;
  arquivo.close();
  
  return 0;
}
